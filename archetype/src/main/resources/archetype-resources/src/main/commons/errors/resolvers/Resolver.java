package ${package}.commons.errors.resolvers;

import ${package}.commons.errors.domains.DefaultErrorResponse;

public interface Resolver<T extends Throwable> {
  DefaultErrorResponse getErrorResponse(T e);
}
